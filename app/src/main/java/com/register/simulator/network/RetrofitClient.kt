@file:Suppress("unused", "unused")

package com.register.simulator.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Suppress("unused", "unused")
object RetrofitClient {
    private const val BASE_URL = "https://market-final-project.herokuapp.com/"

    val instance : RetrofitService by lazy {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        retrofit.create(RetrofitService::class.java)
    }


}